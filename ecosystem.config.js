module.exports = {
    apps: [{
        name: 'tutorial-pt-2',
        script: '../index.js'
    }],
    deploy: {
        production: {
            user: 'ubuntu',
            host: 'ec2-35-158-246-219.eu-central-1.compute.amazonaws.com',
            key: '~/.ssh/YoumeAws01.pem',
            ref: 'origin/master',
            repo: 'git@bitbucket.org:sagil/tutorial-pt-2.git ',
            path: '/home/ubuntu/tutorial-pt-2',
            'post-deploy': 'npm install && pm2 startOrRestart ~/tutorial-pt-2/ecosystem.config.js'
        }
    }
}